package main.java;

public class BoardSquare implements Square {

	public static final String EMPTY_BLACK = "[###]";
	
	private String value;
	private int col;
	private int row;
	
	public BoardSquare() {
	}
	
	@Override
	public String getValue() {
		
		return value;
	}

	@Override
	public void setValue(String value) {
		
		this.value = value;
	}

	@Override
	public int getCol() {
		
		return col;
	}

	@Override
	public void setCol(int col) {
		
		this.col = col;
	}

	@Override
	public int getRow() {
		
		return row;
	}

	@Override
	public void setRow(int row) {
		
		this.row = row;
	}

	@Override
	public void printInfo() {
		
		System.out.printf("%5s\t%s\t%s\t", this.value, this.col, this.row);
	}

}

package main.java;

public interface Square {

	static final String EMPTY = "[   ]";
	
	String getValue();
	void setValue(String value);
	
	int getCol();
	void setCol(int col);
	
	int getRow();
	void setRow(int row);
	
	void printInfo();
}

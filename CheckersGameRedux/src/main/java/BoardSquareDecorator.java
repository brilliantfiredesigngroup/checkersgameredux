package main.java;

public class BoardSquareDecorator implements Square {

	
	
	protected Square square;
	
	private int color = 0;
	private boolean isKing = false;
	
	public BoardSquareDecorator( Square square ) {
		this.square = square;
	}

	public int getColor() {
		return color;
	}
	
	public boolean isKing() {
		return this.isKing;
	}
	
	@Override
	public String getValue() {
		
		return this.square.getValue();
	}

	@Override
	public void setValue(String value) {
		if ( value.equals(KingBoardPiece.PIECE_RED_KING) || value.equals(BoardPiece.PIECE_RED )) {
			this.color = -1;
		} else if ( value.equals(KingBoardPiece.PIECE_BLACK_KING) || value.equals(BoardPiece.PIECE_BLACK) ) {
			this.color = 1;
		} else {
			this.color = 0;
		}
		
		this.square.setValue(value);
	}

	@Override
	public int getCol() {
		
		return this.square.getCol();
	}

	@Override
	public void setCol(int col) {
		
		this.square.setCol(col);
	}

	@Override
	public int getRow() {
		
		return this.square.getRow();
	}

	@Override
	public void setRow(int row) {
		this.square.setRow(row);
	}

	@Override
	public void printInfo() {
		this.square.printInfo();
		System.out.println("Color: " + getColorName() + " Is King: " + isKing());
	}
	
	private String getColorName() {
		String color = "";
		switch (this.color) {
		case -1:
			color = "Red";
			break;
		case 1:
			color = "Black";
			break;
		default:
			color = "";
			break;
		}
		return color;
	}
}

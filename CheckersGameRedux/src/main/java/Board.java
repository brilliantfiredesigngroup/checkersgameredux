package main.java;

public class Board {
	// A static instance is created for strings that are used multiple
	// times to reduce unnecessary memory usage
	private static final String BOARD_LABEL = ("    0     1     2     3     4     5     6     7 ");
	
	// An object array to hold the game pieces
	private Object[][] gameBoard = new Object[8][8];
	
	// Empty constructor 
	public Board() {
	}

	/**
	 * Method to demonstrate creating a board with all elements
	 * empty and has no squares colored.
	 * 
	 * @return an instance of this to allow chaining.
	 */
	public Board initEmptyGameBoard() {
		for ( int i = 0; i < 8; i++ ) { // row
        	
	    		for(int j = 0; j < 8; j++ ) { // column
	    			BoardSquare square = new BoardSquare(); 
    				square.setCol(j);
    				square.setRow(i);
    				square.setValue(BoardSquare.EMPTY);
	    			gameBoard[i][j] = square;
	    		}
	    }
		return this;
	}
	
	/**
	 * Method to demonstrate a game board that has the black
	 * squares colored but does not have player pieces marked
	 * 
	 * @return an instance of this to allow chaining.
	 */
	public Board initEmptyColoredGameBoard() {
		for ( int i = 0; i < 8; i++ ) { // row
        	
	    		for(int j = 0; j < 8; j++ ) { // column
	    			BoardSquare square = new BoardSquare(); 
    				square.setCol(j);
    				square.setRow(i);
    				
	    			if ( i % 2 == 0 ) { // even
	    				
	        			if ( j % 2 == 0 ) { // even
	        				square.setValue(BoardSquare.EMPTY);
	        				gameBoard[i][j] = square;
	        				
	        				
	        			} else { // odd
	        				
	        				square.setValue(BoardSquare.EMPTY_BLACK);
	        				gameBoard[i][j] = square;
	        			}
	    			} else { // odd
	    				if ( j % 2 != 0 ) { // odd
	    					square.setValue(BoardSquare.EMPTY);
	        				gameBoard[i][j] = square;
	        				
	        			} else { // even
	        				
	        				square.setValue(BoardSquare.EMPTY_BLACK);
	        				gameBoard[i][j] = square;
	        			}
	    			}
	    			
	    		}
	    }
		return this;
	}
	
	/**
	 * Method to initialize the game board with the object array 
	 * filled with player pieces and has the black squares colored.
	 * 
	 * @return an instance of this to allow chaining.
	 */
	public Board initGameBoard() {
		for ( int i = 0; i < 8; i++ ) { // row
        	
	    		for(int j = 0; j < 8; j++ ) { // column
	    			BoardSquare square = new BoardSquare(); 
    				square.setCol(j);
    				square.setRow(i);
	    			if ( i % 2 == 0 ) { // even
	    				
	        			if ( j % 2 == 0 ) { // even
	        				
	        				if ( i <= 2 ) {
	        					square.setValue(BoardPiece.PIECE_RED);
	        					gameBoard[i][j] = square; //new Square(BoardPiece.PIECE_RED, j, i);
	        				} else if ( i >= 5 ) {
	        					square.setValue(BoardPiece.PIECE_BLACK);
	        					gameBoard[i][j] = square;//new Square(BoardPiece.PIECE_BLACK, j, i);
	        				} else {
	        					square.setValue(BoardSquare.EMPTY);
	        					gameBoard[i][j] = square;
	        				}
	        				
	        			} else { // odd
	        				square.setValue(BoardSquare.EMPTY_BLACK);
	        				gameBoard[i][j] = square; //new Square(Square.Style.EMPTY_BLACK, j, i);
	        			}
	    			} else { // odd
	    				if ( j % 2 != 0 ) { // odd
	    					if ( i <= 2 ) {
	    						square.setValue(BoardPiece.PIECE_RED);
	        					gameBoard[i][j] = square; //new Square(Square.Style.PIECE_RED, j, i);
	        				} else if ( i >= 5 ) {
	        					square.setValue(BoardPiece.PIECE_BLACK);
	        					gameBoard[i][j] = square; //new Square(Square.Style.PIECE_BLACK, j, i);
	        				} else {
	        					square.setValue(BoardSquare.EMPTY);
	        					gameBoard[i][j] = square; //new BoardSquare();
	        				}
	        			} else { // even
	        				square.setValue(BoardSquare.EMPTY_BLACK);
	        				gameBoard[i][j] = square; //new Square(Square.Style.EMPTY_BLACK, j, i);
	        			}
	    			}
	    			
	    		}
	    }
		return this;
	}
	
	
	@Override
	public String toString() {
		if ( null != gameBoard[0][0] ) { // make sure our array is initialized and is populated
			StringBuffer sb = new StringBuffer();
			sb.append(BOARD_LABEL + "\n\n");
			for ( int i = 0; i < 8; i++ ) {
	        		sb.append(i + " ");
		    		for(int j = 0; j < 8; j++ ) {
		    			Square sq = (Square) gameBoard[i][j];
		    			sb.append(sq.getValue() + " ");
		    		}
		    		sb.append(i + "\n\n");
		    }
			sb.append(BOARD_LABEL);
			return sb.toString();
		} else { // game board not populated
			return "No game board initialized yet.";
		}
	}
	
	
}

package main.java;

/**
 * Class to demonstrate using objects in the board array.
 * NOTE:  Most of the field values can be pulled out of this class and be represented
 * 		  as a Piece object.  I did not create this class to reduce code for 
 * 		  the purpose of demonstrating an answer to your questions.
 * @author Wayne Erdman
 *
 */
public class OldBoardSquare {

	// Enum used only to make reading easier.  You will likely define a piece
	// and use your decorator pattern to decorate a square, king or no king.
	public enum Style {
		  EMPTY {
			  public String toString() {
				  return "[   ]";
			  }
		  },
		  
		  EMPTY_BLACK {
			  public String toString() {
				  return "[###]";
			  }
		  },
		  
		  PIECE_RED {
			  public String toString() {
				  return "[ r ]";
			  }
		  },
		  
		  PIECE_RED_KING {
			  public String toString() {
				  return "[ R ]";
			  }
		  },
		  
		  PIECE_BLACK {
			  public String toString() {
				  return "[ b ]";
			  }
		  },
		  
		  PIECE_BLACK_KING {
			  public String toString() {
				  return "[ B ]";
			  }
		  },
	}
	
	// Array representing the board
//    public static Square[][] grid = new Square[8][8];
    
	private String value = OldBoardSquare.Style.EMPTY.toString();
	
	// y value ( usually )
	private int row = 0;
	
	// x value ( usually )
	private int col = 0;
	
	// for info about piece
	private boolean isKing = false;
	
	// for info about piece color 
	// ( using enum masks this a bit but can be wrote as method on enum if desired )
	// -1 = red, 1 = black, 0 = no piece
	private int color = 0;
	
	public OldBoardSquare() {
		
	}
	
	/**
	 * Constructor to populate field values.  
	 * NOTE:  It can be argued that using multiple constructors to configure
	 *        your objects can make code difficult to maintain.  It is smart
	 *        to leverage getters and setters which are defined by an interface.
	 *        I have left off the getters and setters along with the interface
	 *        to reduce code to facilitate explaining.  This is not how I 
	 *        would go about this normally.
	 *        
	 * @param style
	 * @param col ( x value )
	 * @param row ( y value )
	 */
	public OldBoardSquare(OldBoardSquare.Style style, int col, int row) {
		this.value = style.toString();
		this.col = col;
		this.row = row;
		System.out.println("Creating: " + style.name() + " at (" + col + "," + row + ")");
		
		readForPieceInfo(style);
		
	}
	
	/**
	 * private method only used to populate piece info, this would be
	 * handled on the piece object.
	 * @param style
	 */
	private void readForPieceInfo(Style style) {
		if ( style.name().contains("KING") ) {
			this.isKing = true;
		}
		
		if ( style.name().contains("PIECE") ) {
			if ( style.name().contains("RED") ) {
				this.color = -1;
			}
			
			if ( style.name().contains("BLACK") ) {
				this.color = 1;
			}
		} else {
			this.color = 0;
		}
	}

	// Getters and Setters, some setters omitted due to not using actual piece class and
	// the enum
	
	public String getValue() {
		return this.value;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public boolean isKing() {
		return isKing;
	}

	public int getColor() {
		return color;
	}
	
}

package main.java;

public class KingBoardPiece extends BoardSquareDecorator {

	public static final String PIECE_RED_KING = "[ R ]";
	public static final String PIECE_BLACK_KING = "[ B ]";
	
	public KingBoardPiece(Square square) {
		super(square);
	}
	
	@Override
	public boolean isKing() {
		return true;
	}

	@Override
	public void printInfo() {
		super.printInfo();
	}

	
}

package main.java;

public class Game {

	public static void main(String[] args) {
		
		// Demonstrate the creation of each kind of block
		System.out.println("====================================================");
		System.out.println("Demonstration of pieces:");
		System.out.println("____________________________________________________");
		// [   ]
		BoardPiece emptySquare = new BoardPiece(new BoardSquare());
		emptySquare.setValue(BoardPiece.EMPTY);
		System.out.println(emptySquare.getValue());
		
		// [###]
		BoardPiece emptyBlackSquare = new BoardPiece(new BoardSquare());
		emptyBlackSquare.setCol(0);
		emptyBlackSquare.setRow(0);
		emptyBlackSquare.setValue(BoardSquare.EMPTY_BLACK);
		
		System.out.printf("%5s\tColor: %2s\tKing: %s\tColumn: %s\tRow: %s\n", emptyBlackSquare.getValue(), emptyBlackSquare.getColor(), emptyBlackSquare.isKing(), emptyBlackSquare.getCol(), emptyBlackSquare.getRow());
		
		// [ r ]
		BoardPiece redSquare = new BoardPiece(new BoardSquare());
		redSquare.setCol(0);
		redSquare.setRow(0);
		redSquare.setValue(BoardPiece.PIECE_RED);
		System.out.printf("%5s\tColor: %2s\tKing: %s\tColumn: %s\tRow: %s\n", redSquare.getValue(), redSquare.getColor(), redSquare.isKing(), redSquare.getCol(), redSquare.getRow());
		
		// [ R ]
		KingBoardPiece redKingSquare = new KingBoardPiece(new BoardSquare());
		redKingSquare.setCol(0);
		redKingSquare.setRow(0);
		redKingSquare.setValue(KingBoardPiece.PIECE_RED_KING);
		System.out.printf("%5s\tColor: %2s\tKing: %s\tColumn: %s\tRow: %s\n", redKingSquare.getValue(), redKingSquare.getColor(), redKingSquare.isKing(), redKingSquare.getCol(), redKingSquare.getRow());
		
		// [ b ]
		BoardPiece blackSquare = new BoardPiece(new BoardSquare());//Square.Style.PIECE_BLACK, 0, 0);
		blackSquare.setCol(0);
		blackSquare.setRow(0);
		blackSquare.setValue(BoardPiece.PIECE_BLACK);
		System.out.printf("%5s\tColor: %2s\tKing: %s\tColumn: %s\tRow: %s\n", blackSquare.getValue(), blackSquare.getColor(), blackSquare.isKing(), blackSquare.getCol(), blackSquare.getRow());
		
		// [ B ]
		KingBoardPiece blackKingSquare = new KingBoardPiece(new BoardSquare());
		blackKingSquare.setCol(0);
		blackKingSquare.setRow(0);
		blackKingSquare.setValue(KingBoardPiece.PIECE_BLACK_KING);
		System.out.printf("%5s\tColor: %2s\tKing: %s\tColumn: %s\tRow: %s\n", blackKingSquare.getValue(), blackKingSquare.getColor(), blackKingSquare.isKing(), blackKingSquare.getCol(), blackKingSquare.getRow());	
		
		System.out.println();
		
		// Create an uninitialized Board object
		System.out.println("====================================================");
		System.out.println("Uninitialized Board");
		System.out.println("____________________________________________________");
		Board gameBoard = new Board();
		System.out.println(gameBoard);
		System.out.println();
		
		// Create a Board object that is initialized with all blank squares
		System.out.println("====================================================");
		System.out.println("Board with blank squares");
		System.out.println("____________________________________________________");
		gameBoard = new Board().initEmptyGameBoard();
		System.out.println(gameBoard);
		System.out.println();
		
		
		// Create a Board object that is initialized with blank squares and has
		// black squares filled in.
		System.out.println("====================================================");
		System.out.println("Board with no pieces and colored squares");
		System.out.println("____________________________________________________");
		gameBoard = new Board().initEmptyColoredGameBoard();
		System.out.println(gameBoard);
		System.out.println();
		
		// Create a board object that is initialized with player pieces and has
		// black squares filled in.
		// NOTE: In this use a new Board object is not created.  This was done
		// 		 to show that it is not necessary to create a new object as 
		//		 the method rewrites the array
		System.out.println("====================================================");
		System.out.println("Fully initialized Board");
		System.out.println("____________________________________________________");
		System.out.println(gameBoard.initGameBoard());
		System.out.println();
	}
}

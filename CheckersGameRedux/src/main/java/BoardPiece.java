package main.java;

public class BoardPiece extends BoardSquareDecorator {

	public static final String PIECE_RED = "[ r ]";
	public static final String PIECE_BLACK = "[ b ]";
	
	public BoardPiece(Square square) {
		super(square);
	}

	@Override
	public int getColor() {
		return super.getColor();
	}

	
}
